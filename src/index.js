import React from 'react';
import { createRoot } from 'react-dom/client';
import { Provider } from 'react-redux';
// import { store } from './app/store';
import store from './redux';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './index.css';
// import { ReactDOM } from 'react';
import ReactDOM from 'react-dom';
//path store nya beda dg reading material
import Person from './components/Person';

const container = document.getElementById('root');
const root = createRoot(container);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
);

ReactDOM.render(
  <Provider store={store}>
    <h1>Hello World</h1>
    <Person />
  </Provider>,
  document.getElementById('root')
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
