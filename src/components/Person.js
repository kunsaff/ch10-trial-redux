import React, { Component, Fragment } from "react";
import { connect } from 'react-redux';
class Person extends Component {
    componentDidMount() {
        console.log('Data dari Redux Store')
        console.log('Data:', this.props.data)
    }
    render() {
        const {children} = this.props
        return (
            <div className="container">{children}</div>
        )
    }
}

//lanjutan
//fungsi ini kita pakai untuk melanjutkan 
//mapping dari store ke props dari component
const mapStateToProps = state => ({
    //state adl store dr redux
    //dan persons adalah reducer dr kita
    //yg sdh kita daftarkan mll combineReducers
    data: state.persons
})

export default connect(mapStateToProps)(Person)