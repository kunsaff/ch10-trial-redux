export const ADD = 'person/ADD'

dispatch({
    type: 'person/ADD',
    payload: {
        name: 'Ryan Gosling',
        adress: 'Loz Feliz, California, U.S.',
        phoneNumber: '+1-123-123'
    }
})